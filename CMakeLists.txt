cmake_minimum_required(VERSION 3.16.3)
project(lab4)

set(CMAKE_CXX_STANDARD 11)


set(GCC_COVERAGE_COMPILE_FLAGS "-pthread")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${GCC_COVERAGE_COMPILE_FLAGS}" )

SET(CMAKE_CXX_FLAGS -pthread)

add_executable(main main.cpp src/SharedMem.cpp include/Queue.hpp)