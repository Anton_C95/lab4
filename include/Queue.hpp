//
// Created by anton on 1/9/2022.
//

#ifndef UNTITLED_QUEUE_H
#define UNTITLED_QUEUE_H


#include <iostream>
#include <mutex>
#include <condition_variable>

#define SIZE 10   /* Size of Circular Queue */

using namespace std;

class Queue {
private:
    int items[SIZE], front, rear;
    int currentSize = 0;
    mutable std::mutex guard;
    std::condition_variable cv;
public:
    int getCurrentSize() const {
        return currentSize;
    }

public:
    Queue(){
        front = -1;
        rear = -1;
    }

    bool isFull(){
        if(front == 0 && rear == SIZE - 1){
            return true;
        }
        if(front == rear + 1) {
            return true;
        }
        return false;
    }

    bool isEmpty(){
        if(front == -1) return true;
        else return false;
    }

    void enQueue(int element) {
        //std::unique_lock<std::mutex> uniqueLock(guard);
        //cv.wait(uniqueLock);
        if(front == -1) front = 0;
        rear = (rear + 1) % SIZE;
        items[rear] = element;
        currentSize++;
       // uniqueLock.unlock();
        //cv.notify_one();

    }

    int deQueue(){

        //std::unique_lock<std::mutex> uniqueLock(guard);
        //cv.wait(uniqueLock);
        int element;

        element = items[front];
        if(front == rear){
            front = -1;
            rear = -1;
        } /* Q has only one element, so we reset the queue after deleting it. */
        else {
            front=(front+1) % SIZE;
        }
        currentSize--;
        //uniqueLock.unlock();
        //cv.notify_one();
        return(element);
    }

    void display()
    {
        /* Function to display status of Circular Queue */
        int i;
        if(isEmpty()) {
            cout << endl << "Empty Queue" << endl;
        }
        else
        {
            cout << "Front -> " << front;
            cout << endl << "Items -> ";
            for(i=front; i!=rear;i=(i+1)%SIZE)
                cout << items[i] << ", ";
            cout << items[i];
            cout << endl << "Rear -> " << rear;
        }
    }

};

#endif //UNTITLED_QUEUE_H
