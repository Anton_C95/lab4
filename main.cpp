#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <string>
#include <sys/shm.h>
#include "include/Queue.hpp"
#include "include/SharedMem.h"
#include <new>
#include <wait.h>
#include <random>
#include <error.h>
#include <cstring>

// set follow-fork-mode child
// set detach-on-fork off
typedef struct
{
    sem_t itemsAvailable;
    sem_t spaceAvailable;
    Queue queue;
    pthread_mutex_t mutex;
    pthread_cond_t condProducer;
    pthread_cond_t condConsumer;
} queueStruct;

void startUp(SharedMem sharedMem)
{
    auto* queueStruct = reinterpret_cast<::queueStruct *> (sharedMem.getAddr());

    sem_init(&queueStruct->spaceAvailable, true, 10);
    sem_init(&queueStruct->itemsAvailable, true, 0);

    queueStruct->mutex = PTHREAD_MUTEX_INITIALIZER;
    queueStruct->condProducer = PTHREAD_COND_INITIALIZER;
    queueStruct->condConsumer = PTHREAD_COND_INITIALIZER;
}

// *** https://elearn20.miun.se/moodle/mod/forum/discuss.php?d=506186
int main(int argc, char *argv[]) {

    int numberOfInts;
    if (argc > 1)
        numberOfInts = atoi(argv[1]);
    else
        numberOfInts = 50;

    SharedMem sharedMem(IPC_PRIVATE, sizeof(queueStruct));

    sharedMem.attach();
    startUp(sharedMem);

    new(sharedMem.getAddr()) queueStruct;

    pid_t childpid;
    if ((childpid = fork()) == -1) {
        perror("Error forking");
        return -1;
    } else if (childpid == 0) {

        auto *queueStruct = reinterpret_cast<::queueStruct *> (sharedMem.getAddr());

        for (int i = 0; i < numberOfInts; i++) {
            pthread_mutex_lock(&queueStruct->mutex);
            sem_wait(&queueStruct->itemsAvailable);
            pthread_cond_wait(&queueStruct->condConsumer, &queueStruct->mutex);
            std::cout << "Consumed: " << queueStruct->queue.deQueue() << " nBuffer: "
                      << queueStruct->queue.getCurrentSize() << std::endl;
            sem_post(&queueStruct->spaceAvailable);
            pthread_mutex_unlock(&queueStruct->mutex);
            pthread_cond_signal(&queueStruct->condProducer);
        }

        if ((sharedMem.detach()) == 0) {
            printf("sharedMem detached successfully for consumer\n");
        }
    }

    if (childpid != 0) {

        auto *queueStruct = reinterpret_cast<::queueStruct *> (sharedMem.getAddr());

        std::default_random_engine generator;
        std::uniform_int_distribution<int> distribution(1, 99);

        for (int i = 0; i < numberOfInts; i++) {
            pthread_mutex_lock(&queueStruct->mutex);
            sem_wait(&queueStruct->spaceAvailable);
            pthread_cond_wait(&queueStruct->condProducer, &queueStruct->mutex);
            int value = distribution(generator);
            queueStruct->queue.enQueue(value);
            std::cout << "Produced: " << value << " nBuff: "
                      << queueStruct->queue.getCurrentSize() << std::endl;
            sem_post(&queueStruct->itemsAvailable);
            pthread_mutex_unlock(&queueStruct->mutex);
            pthread_cond_signal(&queueStruct->condConsumer);
        }

        int status;
        while ((childpid = wait(&status)) > 0) {
            if (childpid == -1)
                perror("Failed to wait for child");
            else if (WIFEXITED(status) && !WEXITSTATUS(status))
                printf("Child %ld terminated with return status %d\n",
                       (long) childpid, WEXITSTATUS(status));
            else if (WIFEXITED(status))
                printf("Child %ld terminated with return status %d\n",
                       (long) childpid, WEXITSTATUS(status));
            else if (WIFSIGNALED(status))
                printf("Child %ld terminated due to uncaught signal %d\n",
                       (long) childpid, WTERMSIG(status));
            else if (WIFSTOPPED(status))
                printf("Child %ld stopped due to signal %d\n",
                       (long) childpid, WSTOPSIG(status));
        }
        pthread_mutex_destroy(&queueStruct->mutex);
        if ((sharedMem.detach()) == 0) {
            printf("sharedMem detached successfully for producer\n");
        }
        if ((sharedMem.remove()) == 0) {
            printf("sharedMem deallocated successfully\n");
        }

        if (pthread_cond_destroy(&queueStruct->condProducer))
            fprintf(stderr, "Failed to destroy tcond:%s\n");
        if (pthread_cond_destroy(&queueStruct->condConsumer))
            fprintf(stderr, "Failed to destroy tcond:%s\n");

        return 0;
    }
}